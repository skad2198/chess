/**
 * 
 */
package chess;


/**
 * <code>Knight</code> class extends Piece to create an object of the King. 
 * @author angad
 * @author suraj
 *
 */
public class Knight extends Piece
{
	public Knight(boolean team)
	{
		this.team = team;
		if(team)
		{
			name = "w" + "N";
		}
		else
		{
			name = "b" + "N";
		}
	}
	
	/** 
	 * validMove takes in the Source, Destination of the Piece's move and returns true if it is a valid move for Bishop.
	 * @param X is the x-coordinate of the piece to move from
	 * @param Y is the y-coordinate of the piece to move from
	 * @param toX is the x-coordinate of the piece to move to
	 * @param toY is the y-coordinate of the piece to move to
	 * 
	 * @return true if the move is valid or false if not. 
	 * 
	 */
	@Override
	public boolean validMove(int X, int Y, int toX, int toY, Piece[][] board) 
	{
		int deltaX = Math.abs(toX - X);
		int deltaY = Math.abs(toY - Y);
		//Is the desired block on the board?
		if(toX > 7 || toY > 7 || toX < 0 || toY < 0)
		{
			//System.out.println(name + "Can't Move off the board");
			return false;
		}
		//Are you trying to move to the same block
		if(X == toX && Y == toY)
		{
			//System.out.println(name + "You are already on this block");
			return false;
		}
		//Are you moving to a block with a piece from same team?
		if(board[toX][toY] != null)
		{
			if(board[toX][toY].getTeam() == this.getTeam())
			{
				//System.out.println(name + "You cannot take out a piece on your own team");
				return false;
			}
		}
		//If you make legal L move, then return true
		if((deltaX == 1 && deltaY == 2) || (deltaX == 2 && deltaY == 1))
		{
			//System.out.println(name + "kNIGHT CAN MAKE THIS MOVE");
			return true;
		}
		else
		{
			//System.out.println(name + "Not a valid move for knight");
			return false;
		}
	}
}
