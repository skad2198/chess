/**
 * 
 */
package chess;

/**
 * A Bishop object that extends class Piece
 * 
 * A <code>Bishop</code> object contains parameters and functionality of a Bishop piece in the game of chess.
 * @author angad
 * @author suraj
 * 
 */
public class Bishop extends Piece
{
	
	public Bishop(boolean team)
	{
		this.team = team;
		if(team)
		{
			name = "w" + "B";
		}
		else
		{
			name = "b" + "B";
		}
	}
	
	
	/** 
	 * validMove takes in the Source, Destination of the Piece's move and returns true if it is a valid move for Bishop.
	 * @param X is the x-coordinate of the piece to move from
	 * @param Y is the y-coordinate of the piece to move from
	 * @param toX is the x-coordinate of the piece to move to
	 * @param toY is the y-coordinate of the piece to move to
	 * 
	 * @return true if the move is valid or false if not. 
	 * 
	 */
	
	@Override
	public boolean validMove(int X, int Y, int toX, int toY, Piece[][] board) 
	{
		int deltaX = toX - X;
		int deltaY = toY - Y;
		if(deltaY == 0)
		{
			return false;
		}
		double slope = (double)deltaX/deltaY;
		//Is the desired block on the board?
		if(toX > 7 || toY > 7 || toX < 0 || toY < 0)
		{
			//System.out.println("Can't Move off the board");
			return false;
		}
		//Are you trying to move to the same block
		if(X == toX && Y == toY)
		{
			//System.out.println("You are already on this block");
			return false;
		}
		//If you're trying to go to a spot that contains a piece of your own team
		if(board[toX][toY] != null)
		{
			if(board[toX][toY].team == this.team)
			{
				//System.out.println("A piece of your team is currently occupying that spot");
				return false;
			}
		}
		
		//positive slope
		if(slope == 1)
		{
			//is there a piece in the way of the spot you re trying to move to
			int i = X;
			int j = Y;
			if(isNegative(deltaX))
			{
				i--;
				j--;
			}
			else
			{
				i++;
				j++;
			}
			while((i != toX) && (j != toY))
			{
				if(board[i][j] != null)
				{
					//System.out.println(name + "A piece is in the path between the BISHOP and the desired spot");
					return false;
				}
				if(isNegative(deltaX))
				{
					i--;
					j--;
				}
				else
				{
					i++;
					j++;
				}
			}
			//System.out.println(name + "BISHOP CAN MAKE THIS MOVE");
			return true;
		}
		//negative slope
		else if(slope == -1)
		{
			//is there a piece in the way of the spot you re trying to move to
			int i = X;
			int j = Y;
			if(isNegative(deltaX))
			{
				i--;
				j++;
			}
			else
			{
				i++;
				j--;
			}
			while((i != toX) && (j != toY))
			{
				if(board[i][j] != null)
				{
					//System.out.println(name + "A piece is in the path between the BISHOP and the desired spot");
					return false;
				}
				if(isNegative(deltaX))
				{
					i--;
					j++;
				}
				else
				{
					i++;
					j--;
				}
			}
			//System.out.println(name + "BISHOP CAN MAKE THIS MOVE");
			return true;
		}
		else
		{
			//System.out.println(name + "Not a valid move for the BISOP");
			return false;
		}

		
	}
	
	/**
	 * isNegative returns if the number is negative.
	 * @param num is either deltaX or deltaY
	 * @return true if the number is negative and false if its not.
	 * */
	private boolean isNegative(int num)
	{
		int res = num/Math.abs(num);
		if(res == -1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
