/**
 * 
 */
package chess;

/**
 * The <code>Rook</code> class is an extension of the Piece to create an object of the Rook.
 * @author angad
 * @author suraj
 *
 */
public class Rook extends Piece
{
	public Rook(boolean team)
	{
		this.team = team;
		if(team)
		{
			name = "w" + "R";
		}
		else
		{
			name = "b" + "R";
		}
	}

	/** 
	 * validMove takes in the Source, Destination of the Piece's move and returns true if it is a valid move for Bishop.
	 * @param X is the x-coordinate of the piece to move from
	 * @param Y is the y-coordinate of the piece to move from
	 * @param toX is the x-coordinate of the piece to move to
	 * @param toY is the y-coordinate of the piece to move to
	 * 
	 * @return true if the move is valid or false if not. 
	 * 
	 */
	@Override
	public boolean validMove(int X, int Y, int toX, int toY, Piece[][] board) 
	{
		int deltaX = toX - X;
		int deltaY = toY - Y;
		//Is the desired block on the board?
		if(toX > 7 || toY > 7 || toX < 0 || toY < 0)
		{
			//System.out.println("Can't Move off the board");
			return false;
		}
		//Are you trying to move to the same block
		if(X == toX && Y == toY)
		{
			//System.out.println("You are already on this block");
			return false;
		}
		//Moving Horizontally
		if(deltaX == 0)
		{
			//If you're trying to go to a spot that contains a piece of your own team
			if(board[toX][toY] != null)
			{
				if(board[toX][toY].team == this.team)
				{
					//System.out.println(name + "A piece of your team is currently occupying that spot");
					return false;
				}
			}
			//Is there a piece in the way of the spot you are trying to move to
			int i = Y;
			if(isNegative(deltaY))
			{
				i--;
			}
			else
			{
				i++;
			}
			while(i != toY)
			{
				if(board[X][i] != null)
				{
					//System.out.println(name + "A piece is in the path between the ROOK and the desired spot");
					return false;
				}
				if(isNegative(deltaY))
				{
					i--;
				}
				else
				{
					i++;
				}
			}
			//System.out.println(name + "ROOK CAN MAKE THIS MOVE");
			return true;
		}
		//Moving Vertically
		if(deltaY == 0)
		{
			//If you're trying to go to a spot that contains a piece of your own team
			if(board[toX][toY] != null)
			{
				if(board[toX][toY].team == this.team)
				{
					//System.out.println(name + "A piece of your team is currently occupying that spot");
					return false;
				}
			}

			//Is there a piece in the way of the spot you are trying to move to
			int i = X;
			if(isNegative(deltaX))
			{
				i--;
			}
			else
			{
				i++;
			}
			while(i != toX)
			{
				if(board[i][Y] != null)
				{
					//System.out.println(name + "A piece is in the path between the ROOK and the desired spot");
					return false;
				}
				if(isNegative(deltaX))
				{
					i--;
				}
				else
				{
					i++;
				}
			}
			//System.out.println(name + "ROOK CAN MAKE THIS MOVE");
			return true;

		}
		//System.out.println(name + "Not a valid move for the ROOK");
		return false;
	}
	/**
	 * isNegative returns if the number is negative.
	 * @param num is either deltaX or deltaY
	 * @return true if the number is negative and false if its not.
	 * */
	private boolean isNegative(int num)
	{
		int res = num/Math.abs(num);
		if(res == -1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
