  
/**
 * 
 */
package chess;

/**
 * <code>King</code> class extends Piece to create an object of the King. 
 * @author angad
 *
 */
public class King extends Piece
{
	public King(boolean team)
	{
		this.team = team;
		if(team)
		{
			name = "w" + "K";
		}
		else
		{
			name = "b" + "K";
		}
	}
	
	
	/** 
	 * validMove takes in the Source, Destination of the Piece's move and returns true if it is a valid move for Bishop.
	 * @param X is the x-coordinate of the piece to move from
	 * @param Y is the y-coordinate of the piece to move from
	 * @param toX is the x-coordinate of the piece to move to
	 * @param toY is the y-coordinate of the piece to move to
	 * 
	 * @return true if the move is valid or false if not. 
	 * 
	 */
	@Override
	public boolean validMove(int X, int Y, int toX, int toY, Piece[][] board) 
	{
		int deltaX = toX - X;
		int deltaY = toY - Y;
		//Is the desired block on the board?
		if(toX > 7 || toY > 7 || toX < 0 || toY < 0)
		{
			//System.out.println("Can't Move off the board");
			return false;
		}
		//Are you trying to move to the same block
		if(X == toX && Y == toY)
		{
			//System.out.println("You are already on this block");
			return false;
		}
		//If you're trying to go to a spot that contains a piece of your own team
		if(board[toX][toY] != null)
		{
			if(board[toX][toY].team == this.team)
			{
				//System.out.println(name + "KING: A piece of your team is currently occupying that spot");
				return false;
			}
		}
		//Is the move more than one block away
		if(Math.abs(deltaX) > 1 || Math.abs(deltaY) > 1)
		{
			//System.out.println(name + "King can;t move tht far");
			return false;
		}
		//System.out.println(name + "KING CAN MAKE THIS MOVE");
		return true;
	}
}
