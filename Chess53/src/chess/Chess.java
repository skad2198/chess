/**
 * 
 * */
package chess;

import java.util.Arrays;
import java.util.Scanner;


/**
 * <code>Chess</code> contains the main method and is where you can execute the game. 
 * @author angad
 * @author suraj
 *
 */
public class Chess 
{
	private static Board b;
	private static Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args) 
	{
		//Initialize board
		b = new Board();
		
		//determines whose turn, true = white, black = false;
		boolean turn = true;
		
		//Main Game Loop
		while(true)
		{
			String promotion = null;
			
			//Print out Board
			b.printBoard();
			//Get Input
			String input;
			if(turn)
			{
				System.out.print("White's move: ");
			}
			else
			{
				System.out.print("Black's move: ");
			}
			input = scan.nextLine();
			
			//Interpret input
			int X = 0, Y = 0, toX = 0, toY = 0;
			String alpha = "abcdefgh";
			String[] res = input.split(" ");
			
			int len = res.length;
			if(len == 1)
			{
				String temp = res[0];
				if(temp.equals("resign"))
				{
					if(turn)
					{
						System.out.println("Black Wins");
						return;
					}
					else
					{
						System.out.println("White Wins");
						return;
					}
				}
				else
				{
					return;
				}
			}
			if(len == 2)
			{
				String temp = res[0];
				X = Character.getNumericValue(temp.charAt(1)) - 1;
				Y = alpha.indexOf(temp.charAt(0));
				temp = res[1];
				toX = Character.getNumericValue(temp.charAt(1)) - 1;
				toY = alpha.indexOf(temp.charAt(0));
				//System.out.println(X + " " + Y + " " + toX + " " + toY);
			}
			if(len == 3)
			{
				String temp = res[2];
				if(temp.equals("draw?"))
				{
					temp = res[0];
					X = Character.getNumericValue(temp.charAt(1)) - 1;
					Y = alpha.indexOf(temp.charAt(0));
					temp = res[1];
					toX = Character.getNumericValue(temp.charAt(1)) - 1;
					toY = alpha.indexOf(temp.charAt(0));
					//System.out.println(X + " " + Y + " " + toX + " " + toY);
				}
				else
				{
					promotion = temp;
					temp = res[0];
					X = Character.getNumericValue(temp.charAt(1)) - 1;
					Y = alpha.indexOf(temp.charAt(0));
					temp = res[1];
					toX = Character.getNumericValue(temp.charAt(1)) - 1;
					toY = alpha.indexOf(temp.charAt(0));
					//System.out.println(X + " " + Y + " " + toX + " " + toY);
				}
				
			}
			
			//Is the selected piece part of your team
			if(b.board[X][Y].team != turn)
			{
				//System.out.println("Invalid Move: that piece is not on your team");
				System.out.println("Illegal move, try again");
				continue;
			}
			else
			{
				//move for pawn
				if(b.board[X][Y].getName().contains("P"))
				{
					//If promotion piece isn't specified, it is Queen
					if(b.board[X][Y].getTeam() && X == 7 && promotion == null)
					{
						promotion = "Q";
					}
					if(!b.board[X][Y].getTeam() && X == 0 && promotion == null)
					{
						promotion = "Q";
					}
					
					//no promotion
					if(promotion == null)
					{
						if(b.board[X][Y].validMove(X, Y, toX, toY, b.board))
						{
							if(validBoardMove(X, Y, toX, toY, b.board[X][Y].getTeam()))
							{
								move(X,Y,toX,toY);
								turn = !turn;
							}
							else
							{
								//System.out.println("Invalid Move: that move would put your king in check");
								System.out.println("Illegal move, try again");
								continue;
							}
						}
						else
						{
							//System.out.println("Invalid Move");
							System.out.println("Illegal move, try again");
							continue;
						}
					}
					//Promotion
					else
					{
						//Make appropriate piece for promotion
						Piece p = new Pawn(true);
						if (promotion == "N")
						{
							p = new Knight(b.board[X][Y].getTeam());
						}
						else if (promotion == "R")
						{
							p = new Rook(b.board[X][Y].getTeam());
						}
						else if (promotion == "B")
						{
							p = new Bishop(b.board[X][Y].getTeam());
						}
						else if (promotion == "Q")
						{
							p = new Queen(b.board[X][Y].getTeam());
						}
						if(b.board[X][Y].validMove(X, Y, toX, toY, b.board))
						{
							if(validBoardMove(X, Y, toX, toY, b.board[X][Y].getTeam()))
							{
								//Promote piece if valid move
								b.board[X][Y] = p;
								move(X,Y,toX,toY);
								turn = !turn;
							}
							else
							{
								//System.out.println("Invalid Move: that move would put your king in check");
								System.out.println("Illegal move, try again");
								continue;
							}
						}
						else
						{
							//System.out.println("Invalid Move");
							System.out.println("Illegal move, try again");
							continue;
						}
					}
				}
				//move for king
				else if(b.board[X][Y].getName().contains("K"))
				{
					boolean c = false;
					//Castling
					if(!isChecked(toX, toY, turn, b.board))
					{
						//System.out.println("YOU ARE IN THE ASTLE");
						if((X == 0 && Y == 4 && toX == 0 && toY == 6) && (b.board[0][7] instanceof Rook) && (b.board[0][5] == null && b.board[0][6] == null))
						{
							move(X,Y,toX,toY);
							move(0,7,0,5);
							c = true;
							turn = !turn;
						}
						else if((X == 0 && Y == 4 && toX == 0 && toY == 2) && (b.board[0][0] instanceof Rook) && (b.board[0][2] == null && b.board[0][3] == null))
						{
							move(X,Y,toX,toY);
							move(0,0,0,3);
							c = true;
							turn = !turn;
						}
						else if((X == 7 && Y == 4 && toX == 7 && toY == 6) && (b.board[7][7] instanceof Rook) && (b.board[7][5] == null && b.board[7][6] == null))
						{
							move(X,Y,toX,toY);
							move(7,7,7,5);
							c = true;
							turn = !turn;
						}
						else if((X == 7 && Y == 4 && toX == 7 && toY == 2) && (b.board[7][0] instanceof Rook) && (b.board[7][2] == null && b.board[7][3] == null))
						{
							move(X,Y,toX,toY);
							move(7,0,7,3);
							c = true;
							turn = !turn;
						}

					}
					if(!c)
					{
						if(b.board[X][Y].validMove(X, Y, toX, toY, b.board))
						{
							if(isChecked(toX, toY, turn, b.board))
							{
								//System.out.println("Invalid Move: that move would put your king in check");
								System.out.println("Illegal move, try again");
								continue;
							}
							else
							{
								move(X,Y,toX,toY);
								turn = !turn;
							}
						}
						else
						{
							//System.out.println("Invalid Move");
							System.out.println("Illegal move, try again");
							continue;
						}
					}

				}
				//move for everything else
				else
				{
					//System.out.println(b.board[X][Y].getName());
					if(b.board[X][Y].validMove(X, Y, toX, toY, b.board))
					{
						if(validBoardMove(X, Y, toX, toY, b.board[X][Y].getTeam()))
						{
							move(X,Y,toX,toY);
							turn = !turn;
						}
						else
						{
							//System.out.println("Invalid Move: that move would put your king in check");
							System.out.println("Illegal move, try again");
							continue;
						}
					}
					else
					{
						//System.out.println("Invalid Move");
						System.out.println("Illegal move, try again");
						continue;
					}
				}
			}
			
			//Check for check
			//System.out.println("------Starting Check for CHECKKKKK-------");
			//Find other team's King
			boolean chk = false;
			int kX = 0;
			int kY = 0;
			Piece[][] bo = b.board;
			for(int i = 0; i < bo.length; i++)
			{
				for(int j = 0; j < bo[0].length; j++)
				{
					if(bo[i][j] != null)
					{
						if(bo[i][j].getTeam() == turn && bo[i][j].getName().contains("K"))
						{
							//System.out.println("KING FOUND" + bo[i][j].name + " AT " + i + " " + j);
							kX = i;
							kY = j;

						}
					}

				}
			}
			//Check if king is in check
			for(int i = 0; i < bo.length; i++)
			{
				for(int j = 0; j < bo[0].length; j++)
				{
					if(bo[i][j] != null)
					{
						if(bo[i][j].getTeam() != turn)
						{
							if(bo[i][j].validMove(i, j, kX, kY, bo))
							{
								//System.out.println("THIS PIECE SAYS OK " + bo[i][j].getName());
								chk = true;
							}
						}
					}

				}
			}
			//check for check at every spot immediately around the king
			if(chk)
			{
				boolean checkmate = true;
				if(kX + 1 < 8 && kY < 8 && kX + 1 >= 0 && kY >= 0)
				{
					if(b.board[kX][kY].validMove(kX, kY, kX + 1, kY, b.board))
					{
						if(!(isChecked(kX + 1, kY, turn, b.board)))
						{
							//System.out.println("ONE");
							checkmate = false;
						}
					}

				}
				if(kX < 8 && kY + 1 < 8 && kX >= 0 && kY + 1 >= 0)
				{
					if(b.board[kX][kY].validMove(kX, kY, kX, kY + 1, b.board))
					{
						if(!(isChecked(kX, kY + 1, turn, b.board)))
						{
							//System.out.println("TWO");
							checkmate = false;
						}
					}

				}
				if(kX + 1 < 8 && kY + 1 < 8 && kX + 1 >= 0 && kY + 1 >= 0)
				{
					if(b.board[kX][kY].validMove(kX, kY, kX + 1, kY + 1, b.board))
					{
						if(!(isChecked(kX + 1, kY + 1, turn, b.board)))
						{
							//System.out.println("THREE");
							checkmate = false;
						}
					}
				}
				if(kX - 1 < 8 && kY < 8 && kX - 1 >= 0 && kY >= 0)
				{
					if(b.board[kX][kY].validMove(kX, kY, kX - 1, kY, b.board))
					{
						if(!(isChecked(kX - 1, kY, turn, b.board)))
						{
							//System.out.println("FOUR");
							checkmate = false;
						}
					}

				}
				if(kX < 8 && kY - 1 < 8 && kX >= 0 && kY - 1 >= 0)
				{
					if(b.board[kX][kY].validMove(kX, kY, kX, kY - 1, b.board))
					{
						if(!(isChecked(kX, kY - 1, turn, b.board)))
						{
							//System.out.println("FIVE");
							checkmate = false;
						}
					}
				}
				if(kX - 1 < 8 && kY - 1< 8 && kX - 1 >= 0 && kY - 1 >= 0)
				{
					if(b.board[kX][kY].validMove(kX, kY, kX - 1, kY - 1, b.board))
					{
						if(!(isChecked(kX - 1, kY - 1, turn, b.board)))
						{
							//System.out.println("SIX");
							checkmate = false;
						}
					}
				}
				if(kX + 1 < 8 && kY - 1 < 8 && kX + 1 >= 0 && kY - 1 >= 0)
				{
					if(b.board[kX][kY].validMove(kX, kY, kX + 1, kY - 1, b.board))
					{
						if(!(isChecked(kX + 1, kY - 1, turn, b.board)))
						{
							//System.out.println("SEVEN");
							checkmate = false;
						}
					}
				}
				if(kX - 1 < 8 && kY + 1 < 8 && kX - 1 >= 0 && kY + 1 >= 0)
				{
					if(b.board[kX][kY].validMove(kX, kY, kX - 1, kY + 1, b.board))
					{
						if(!(isChecked(kX - 1, kY + 1, turn, b.board)))
						{
							//System.out.println("EIGHT");
							checkmate = false;
						}
					}
				}
				//check if any other valid moves exist
				if(checkmate)
				{
					Piece[][] boo = b.board;
					for(int i = 0; i < boo.length; i++)
					{
						for (int j = 0; j < boo[0].length; j++)
						{
							if(boo[i][j] != null && !(boo[i][j] instanceof King))
							{
								if(boo[i][j].getTeam() == turn)
								{
									for(int k = 0; k < boo.length; k++)
									{
										for(int l = 0; l < boo[0].length; l++)
										{
											if(boo[i][j].validMove(i, j, k, l, boo))
											{
												if(validBoardMove(i,j,k,l,turn))
												{
													checkmate = false;
													break;
												}
											}
										}
									}
								}
							}
						}
					}
				}
				if(checkmate)
				{
					turn = !turn;
					System.out.println("Checkmate");
					if(turn)
					{
						System.out.println("White wins");
					}
					else
					{
						System.out.println("Black wins");
					}
					return;
				}
				else
				{
					System.out.println("Check");
				}
			}
		}
	}
	
	
	//moves piece from one space to other
	public static void move(int X, int Y, int toX, int toY)
	{
		b.board[toX][toY] = b.board[X][Y];
		b.board[X][Y] = null;
	}
	/**
	 * validBoardMove checks if move will put king into check
	 * @param X is the x-coordinate of the piece to move from
	 * @param Y is the y-coordinate of the piece to move from
	 * @param toX is the x-coordinate of the piece to move to
	 * @param toY is the y-coordinate of the piece to move to
	 * @param team is the boolean value for representing the side (black or white)
	 * 
	 * @return true if there is a check, otherwise false
	 * */
	public static boolean validBoardMove(int X, int Y, int toX, int toY, boolean team)
	{
		//System.out.println("------Starting Check for VLIDBOARDMOVE-------");
		//make move
		Piece[][] bo = clone(b.board);
		bo[toX][toY] = bo[X][Y];
		//System.out.println("THIS PIECE IS NOW A " + bo[toX][toY].name);
		bo[X][Y] = null;
		
		//Is your king checked
		for(int i = 0; i < bo.length; i++)
		{
			for(int j = 0; j < bo[0].length; j++)
			{
				if(bo[i][j] != null)
				{
					if(bo[i][j].getTeam() == team && bo[i][j].getName().contains("K"))
					{
						if(isChecked(i,j,team, bo))
						{
							return false;
						}
					}
				}

			}
		}
		return true;
	}
	/**
	 * isChecked checks if the spot is checked by the other team.
	 * 
	 * @param X is the x-coordinate of the checked spot
	 * @param Y is the y-coordinate of the checked spot
	 * @param team is the boolean value for representing the side of the piece (black or white)
	 * @param board is the 2-D matrix representing the chess board
	 * 
	 * @return true if the spot (x,y) is checked by a piece of the other team, else false.
	 * */
	public static boolean isChecked(int X, int Y, boolean team, Piece[][] board)
	{
		Piece[][] bo = board;
		/*
		if(X >= bo.length || Y >= bo[0].length)
		{
			return true;
		}
		*/
		for(int i = 0; i < bo.length; i++)
		{
			for(int j = 0; j < bo[0].length; j++)
			{
				if(bo[i][j] != null)
				{
					if(bo[i][j].getTeam() != team)
					{
						if(bo[i][j].validMove(i, j, X, Y, bo))
						{
							//System.out.println("THIS PIECE SAYS OK " + bo[i][j].getName());
							return true;
						}
					}
				}

			}
		}
		return false;
	}
	
	/**
	 * clone duplicates the board and returns copy
	 * @param board is the 2-D matrix representing the chess board
	 * 
	 * @return a copy of the board instance
	 * */
	public static Piece[][] clone(Piece[][] board)
	{
		Piece[][] copy = new Piece[8][8];
		for(int i = 0; i < board.length; i++)
		{
			for(int j = 0; j < board[0].length; j++)
			{
				if(board[i][j] == null)
				{
					copy[i][j] = null;
				}
				else
				{
					if(board[i][j] instanceof Pawn)
					{
						copy[i][j] = new Pawn(board[i][j].getTeam());
					}
					else if(board[i][j] instanceof King)
					{
						copy[i][j] = new King(board[i][j].getTeam());
					}
					else if(board[i][j] instanceof Knight)
					{
						copy[i][j] = new Knight(board[i][j].getTeam());
					}
					else if(board[i][j] instanceof Queen)
					{
						copy[i][j] = new Queen(board[i][j].getTeam());
					}
					else if(board[i][j] instanceof Rook)
					{
						copy[i][j] = new Rook(board[i][j].getTeam());
					}
					else if(board[i][j] instanceof Bishop)
					{
						copy[i][j] = new Bishop(board[i][j].getTeam());
					}
				}
			}
		}
		return copy;
	}

}
