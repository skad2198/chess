package chess;

/**
 * <code>Piece</code> is the abstract class for the game, it specifies the Piece object and the functionality of that Piece object. 
 * @author angad
 * @author suraj
 *
 */
public abstract class Piece 
{
	//team True = White, team False = Black
	boolean team;
	//What kind of piece?
	String name;
	
	
	/**
	 * setTeam sets the team of the piece
	 * @param team, a boolean type that denotes true or false for black or white team.
	 * does not return anything. 
	 *
	 */
	public void setTeam(boolean team)
	{
		this.team = team;
	}
	
	/**
	 * getTeam gets the team of the piece
	 * @return true or false, depending on if the piece belongs to black or white team. 
	 *
	 */
	public boolean getTeam()
	{
		return this.team;
	}
	
	/**
	 * getName gets the name of the piece
	 * @return a string type with the name of the piece. 
	 *
	 */
	public String getName()
	{
		return this.name;
	}
	
	/**
	 * setName sets the Name of the piece
	 * @param name that is to be assigned to the piece
	 * does not return anything
	 *
	 */
	public void setName(String name)
	{
		this.name = name;
	}
	
	public abstract boolean validMove(int X, int Y, int toX, int toY, Piece[][] board);
	
	
	public String toString()
	{
		return this.name;
	}
	
	

	
	
}
