package chess;

import java.util.Arrays;

/**
 * 
 *  A <code>board</code> class to setup the board object with the pieces on it. 
 *  It takes no parameters and does not have a return type. 
 *  @author angad
 *  @author suraj
 *  
 * */ 
public class Board 
{
	Piece[][] board;
	
	public Board()
	{
		//Create a 2D array for the chess board
		board = new Piece[8][8];
		setUp();
	}
	
	
	/**
	 * setUp is to initialize the board with pieces in place. 
	 * 
	 * It has no params and no return type.
	 */
	public void setUp()
	{
		for(int i = 0; i < board.length; i++)
		{
			for(int j = 0; j < board[0].length; j++)
			{
				board[i][j] = null;
			}
		}
		
		//Add white pawns
		for(int i = 0; i < board.length;i++)
		{
			board[1][i] = new Pawn(true);
		}
		//Add white back pieces
		board[0][0] = new Rook(true);
		board[0][7] = new Rook(true);
		board[0][2] = new Bishop(true);
		board[0][5] = new Bishop(true);
		board[0][1] = new Knight(true);
		board[0][6] = new Knight(true);
		board[0][3] = new Queen(true);
		board[0][4] = new King(true);
		
		//Add black pawns
		for(int i = 0; i < board.length;i++)
		{
			board[6][i] = new Pawn(false);
		}
		//Add black back pieces
		board[7][0] = new Rook(false);
		board[7][7] = new Rook(false);
		board[7][2] = new Bishop(false);
		board[7][5] = new Bishop(false);
		board[7][1] = new Knight(false);
		board[7][6] = new Knight(false);
		board[7][3] = new Queen(false);
		board[7][4] = new King(false);
	}
	
	
	/**
	 * printboard prints the board whenever required.
	 * 
	 * It has no params and no return type.
	 */
	public void printBoard()
	{
		//get string values for each block
		String[][] sBoard = new String[8][8];
		boolean line = true;
 		for(int i = 0; i < sBoard.length; i++)
		{
 			boolean block = line;
			for(int j = 0; j < sBoard[0].length; j++)
			{
				if(board[i][j] != null)
				{
					sBoard[i][j] = board[i][j].toString();
				}
				else if(block)
				{
					sBoard[i][j] = "  ";
				}
				else
				{
					sBoard[i][j] = "##";
				}
				block = !block;
			}
			line = !line;
		}
 		
 		//print the board
 		System.out.println(" ");
 		for(int i = 7; i >= 0; i--)
		{
 			String row = "";
			for(int j = 0; j < sBoard[0].length; j++)
			{
				row = row + sBoard[i][j] + " ";
			}
			row = row + (i+1);
			System.out.println(row);
			row = "";
		}
 		System.out.println(" a  b  c  d  e  f  g  h");
 		System.out.println(" ");
	}
	
}
