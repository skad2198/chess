/**
 * 
 */
package chess;

/**
 * <code>Pawn</code> class extends Piece to create an object of the King. 
 * @author angad
 * @author suraj
 *
 */
public class Pawn extends Piece
{
	public Pawn(boolean team)
	{
		this.team = team;
		if(team)
		{
			name = "w" + "P";
		}
		else
		{
			name = "b" + "P";
		}
	}

	/** 
	 * validMove takes in the Source, Destination of the Piece's move and returns true if it is a valid move for Bishop.
	 * @param X is the x-coordinate of the piece to move from
	 * @param Y is the y-coordinate of the piece to move from
	 * @param toX is the x-coordinate of the piece to move to
	 * @param toY is the y-coordinate of the piece to move to
	 * 
	 * @return true if the move is valid or false if not. 
	 * 
	 */
	@Override
	public boolean validMove(int X, int Y, int toX, int toY, Piece[][] board) 
	{
		int deltaX = toX - X;
		int deltaY = toY - Y;

		//Is the desired block on the board?
		if(toX > 7 || toY > 7 || toX < 0 || toY < 0)
		{
			//System.out.println("Can't Move off the board");
			return false;
		}
		//Are you trying to move to the same block
		if(X == toX && Y == toY)
		{
			//System.out.println("You are already on this block");
			return false;
		}
		//If you're trying to go to a spot that contains a piece of your own team
		if(board[toX][toY] != null)
		{
			if(board[toX][toY].team == this.team)
			{
				//System.out.println("A piece of your team is currently occupying that spot");
				return false;
			}
		}
		//Are you trying to move backwards
		if(team)
		{
			//White backwards 
			if(toX < X)
			{
				//System.out.println(name + "PAWN Can't go backwards");
				return false;
			}

		}
		else
		{
			//Black Backwards
			if(toX > X)
			{
				//System.out.println(name + "PAWN Can't go backwards");
				return false;
			}
		}
		//Are you trying to move sideways
		if(X == toX && Y != toY)
		{
			//System.out.println(name + "PAWN Can't go sideways");
			return false;
		}
		
		//Moving Vertically
		if(Y == toY)
		{
			//Moving 2 spaces forward
			if(Math.abs(deltaX) == 2)
			{
				//Can only move two spaces in starting position
				if((team && X == 1) || (!team && X == 6))
				{
					//Are there any pieces in the way
					if(board[X + deltaX/2][Y] == null && board[X + deltaX][Y] == null)
					{
						return true;
					}
					else
					{
						//System.out.println(name + "PAWN: Can't move, pieces in the way");
						return false;
					}
				}
				else
				{
					//System.out.println(name + "PAWN: Can't move 2 spaces from this position");
					return false;
				}
			}
			//Moving 1 space forward
			else if(Math.abs(deltaX) == 1)
			{
				//Are there any pieces in the way
				if(board[X + deltaX][Y] == null)
				{
					return true;
				}
				else
				{
					//System.out.println(name + "PAWN: Can't move, pieces in the way");
					return false;
				}
			}
			else
			{
				//System.out.println(name + "PAWN can't move that far");
				return false;
			}
		}
		
		//Moving Diagonally
		if(Math.abs(deltaX) > 0 && Math.abs(deltaY) > 0)
		{
			//Moving diaginally by one block
			if(Math.abs(deltaX) == 1 && Math.abs(deltaY) == 1)
			{
				//if block not empty, valid move
				if(board[X + deltaX][Y + deltaY] != null)
				{
					return true;
				}
				else
				{
					//System.out.println(name + "pAWN: Cannot move there unless taking piece out");
					return false;
				}
			}
			else
			{
				//System.out.println(name + "PAWN: Can't move that far diagonally");
				return false;
			}
		}
		//System.out.println(name + "PAWN CAN MAKE THIS MOVE");
		return true;
	}
	
}
